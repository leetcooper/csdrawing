Building:

	To build the program you will need Java 9 JDK and Gradle I am using this
	setup.  The reason I am using JVM 10.0.1 is that I plan to
	use this project to experiment with number of new java 9 features in the
	future.

	gradle -v

	------------------------------------------------------------
	Gradle 4.7
	------------------------------------------------------------

	Build time:   2018-04-18 09:09:12 UTC
	Revision:     b9a962bf70638332300e7f810689cb2febbd4a6c

	Groovy:       2.4.12
	Ant:          Apache Ant(TM) version 1.9.9 compiled on February 2 2017
	JVM:          10.0.1 ("Oracle Corporation" 10.0.1+10)
	OS:           Windows 10 10.0 amd64

	Build command:
		gradle build jacocoTestReport

	Testing:
		You will notice the jacocTestReport shows 100% code test coverage,  However
		to gold plate the solution a number of extra unit test need to be written.
		The test that best describes the functionality of the system is
		DrawEndToEndTest and it should be noted I decided to use a non blocking
		approach to testing the System.in.

Example Usage:
	[Please forgive the WARNING obviously I am running the bleeding edge JVM
	which results a number of new reflection warnings.]

	Navigate to build\libs

	Run:
		java -jar cs-drawing-1.jar
		WARNING: An illegal reflective access operation has occurred
		WARNING: Illegal reflective access by com.google.inject.internal.cglib.core.$ReflectUtils$1 to method java.lang.ClassLoader.defineClass(java.lang.String,byte[],int,int,java.security.ProtectionDomain)
		WARNING: Please consider reporting this to the maintainers of com.google.inject.internal.cglib.core.$ReflectUtils$1
		WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
		WARNING: All illegal access operations will be denied in a future release
		enter command:
		C 10 10
		************
		|          |
		|          |
		|          |
		|          |
		|          |
		|          |
		|          |
		|          |
		|          |
		|          |
		************
		enter command:
		L 1 1 1 5
		************
		|*         |
		|*         |
		|*         |
		|*         |
		|*         |
		|          |
		|          |
		|          |
		|          |
		|          |
		************
		enter command:

Assumptions:

  1. When a new canvas entered then I am assuming the user wishes to start
     again.

  2. When an object is created that fully or partially falls outside of the
     canvas I am not raising an error and rendering only what can be rendered
     within the canvas.

  3. If the first command is not a Canvas will automatically generate a canvas
     with default dimensions
