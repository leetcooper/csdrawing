package com.cs.drawing;

import com.cs.draw.drawing.CommandExecutor;
import com.cs.draw.model.Canvas;
import com.cs.draw.model.Drawable;
import com.cs.draw.validation.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommandExecutorUnitTest {

    @Mock
    private CommandValidatorParserProvider commandValidatorParser;

    @Mock
    private CommandValidator mockCommandValidator;

    @Mock
    private CommandParser mockCommandParser;

    @Mock
    private Canvas.CanvasConsumer mockCanvasConsumer;

    @Mock
    private CommandExecutor.OnError mockOnError;

    @Mock
    private Canvas mockCanvas;

    @Mock
    private Drawable mockDrawable;

    public static final String COMMAND = "command";

    @Before
    public void setUpMocks(){
        when(commandValidatorParser.getCommandParser()).thenReturn(mockCommandParser);
        when(commandValidatorParser.getCommandValidator()).thenReturn(mockCommandValidator);
    }

    @Test
    public void canRenderCanvas(){
        CommandExecutor commandExecutor =
                CommandExecutor
                        .getInstance(commandValidatorParser, COMMAND);

        when(mockCommandParser
                .parse(COMMAND))
                .thenReturn(mockCanvas);

        when(mockCommandValidator.validate(mockCanvas))
                .thenReturn(ApplicationErrors.empty());

        commandExecutor
                .onValidCommand(mockCanvasConsumer)
                .orElse(mockOnError);

        verify(mockCanvas
                , times(1))
                .renderCanvas(mockCanvasConsumer);
    }

    @Test
    public void canRenderDrawable(){
        CommandExecutor commandExecutor =
                CommandExecutor
                        .getInstance(commandValidatorParser, COMMAND);

        when(mockCommandParser
                .parse(COMMAND))
                .thenReturn(mockDrawable);

        when(mockCommandValidator.validate(mockDrawable))
                .thenReturn(ApplicationErrors.empty());

        commandExecutor
                .onCanvas(mockCanvas)
                .onValidCommand(mockCanvasConsumer)
                .orElse(mockOnError);

        verify(mockCanvas
                , times(1))
                .renderCanvas(mockCanvasConsumer);
    }

    @Test
    public void canHandleApplicationErrors(){
        CommandExecutor commandExecutor =
                CommandExecutor
                        .getInstance(commandValidatorParser, COMMAND);

        when(mockCommandParser
                .parse(COMMAND))
                .thenReturn(mockCanvas);

        ApplicationErrors error = ApplicationErrors
                .builder()
                .addError(ApplicationError.of("C", "ERROR"))
                .build();

        when(mockCommandParser
                .parse(COMMAND))
                .thenReturn(mockDrawable);

        when(mockCommandValidator.validate(mockDrawable))
                .thenReturn(error);

        commandExecutor
                .orElse(mockOnError);

        verify(mockOnError
                , times(1))
                .onError(error);
    }
}
