package com.cs.drawing.model;

import com.cs.draw.model.Dimension;
import com.cs.draw.model.Drawing;
import com.cs.draw.model.Point;
import com.cs.draw.model.Rectangle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RectangleTest {
    @Test
    public void canDrawRectangle() {
        final String horizontal = "*";
        final String vertical = "|";

        final Point point1 = Point.of(0, 0);
        final Point point2 = Point.of(1, 0);
        final Point point3 = Point.of(2, 0);
        final Point point4 = Point.of(0, 1);
        final Point point5 = Point.of(2, 1);
        final Point point6 = Point.of(0, 2);
        final Point point7 = Point.of(1, 2);
        final Point point8 = Point.of(2, 2);

        Rectangle rectangle = Rectangle.of(
                Point.of(0, 0),
                Dimension.of(3, 3),
                Rectangle.RectangleCharacters.of(horizontal, vertical));

        Drawing drawing = rectangle.draw();

        assertEquals(8, drawing.list().size());

        assertAtPoint(point1, drawing, horizontal);
        assertAtPoint(point2, drawing, horizontal);
        assertAtPoint(point3, drawing, horizontal);
        assertAtPoint(point4, drawing, vertical);
        assertAtPoint(point5, drawing, vertical);
        assertAtPoint(point6, drawing, horizontal);
        assertAtPoint(point7, drawing, horizontal);
        assertAtPoint(point8, drawing, horizontal);
    }

    private void assertAtPoint(Point point, Drawing drawing, String c) {
        assertEquals(c, drawing.get(point).get().getCharacter());
    }
}
