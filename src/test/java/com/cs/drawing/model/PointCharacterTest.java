package com.cs.drawing.model;

import com.cs.draw.model.Drawing.PointCharacter;
import com.cs.draw.model.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PointCharacterTest {
    @Test
    public void canEqualsHandleNull() {
        assertFalse(PointCharacter.of(Point.of(0, 0), "c").equals(null));
    }

    @Test
    public void canEqualsHandleDifferentType() {
        assertFalse(PointCharacter.of(Point.of(0, 0), "c").equals(new Object()));
    }

    @Test
    public void canEqualsHandleItSelf() {
        final PointCharacter point = PointCharacter.of(Point.of(0, 0), "c");
        assertTrue(point.equals(point));
    }

    @Test
    public void canEqualsMatchPoints() {
        final PointCharacter point1 = PointCharacter.of(Point.of(0, 0), "c");
        final PointCharacter point2 = PointCharacter.of(Point.of(0, 0), "c");
        assertTrue(point1.equals(point2));
    }

    @Test
    public void canEqualsNotMatchPointY() {
        final PointCharacter point1 = PointCharacter.of(Point.of(0, 0), "c");
        final PointCharacter point2 = PointCharacter.of(Point.of(0, 1), "c");
        assertFalse(point1.equals(point2));
    }

    @Test
    public void canEqualsNotMatchPointX() {
        final PointCharacter point1 = PointCharacter.of(Point.of(0, 0), "c");
        final PointCharacter point2 = PointCharacter.of(Point.of(1, 0), "c");
        assertFalse(point1.equals(point2));
    }

    @Test
    public void canEqualsNotMatchChar() {
        final PointCharacter point1 = PointCharacter.of(Point.of(0, 0), "c");
        final PointCharacter point2 = PointCharacter.of(Point.of(0, 0), "x");
        assertFalse(point1.equals(point2));
    }

    @Test
    public void canMatchHashCode() {
        final PointCharacter point1 = PointCharacter.of(Point.of(0, 0), "c");
        final PointCharacter point2 = PointCharacter.of(Point.of(0, 0), "c");
        assertEquals(point1.hashCode(), point2.hashCode());
    }

}
