package com.cs.drawing.model;

import com.cs.draw.model.Drawing.PointCharacter;
import com.cs.draw.model.MapDrawing;
import com.cs.draw.model.Point;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class MapDrawingTest {
    @Test
    public void canAddRetrievePointCharacter() {
        final Point point = Point.of(1, 1);
        final String c = "X";

        final MapDrawing drawing = new MapDrawing();
        drawing.add(PointCharacter.of(point, c));

        assertEquals(c, drawing.get(point).get().getCharacter());
    }

    @Test
    public void canListPointCharacters() {
        final Point point1 = Point.of(1, 1);
        final String c1 = "X";
        final Point point2 = Point.of(2, 2);
        final String c2 = "Y";

        final MapDrawing drawing = new MapDrawing();
        drawing.add(PointCharacter.of(point1, c1));
        drawing.add(PointCharacter.of(point2, c2));

        final Collection<PointCharacter> points = drawing.list();
        assertEquals(2, points.size());

        assertTrue(points.contains(PointCharacter.of(point1, c1)));
        assertTrue(points.contains(PointCharacter.of(point2, c2)));
    }

    @Test
    public void canAddAll() {
        final Point point1 = Point.of(1, 1);
        final String c1 = "X";
        final Point point2 = Point.of(2, 2);
        final String c2 = "Y";

        final MapDrawing drawing1 = new MapDrawing();
        drawing1.add(PointCharacter.of(point1, c1));
        final MapDrawing drawing2 = new MapDrawing();
        drawing2.add(PointCharacter.of(point2, c2));

        drawing1.addAll(drawing2);

        final Collection<PointCharacter> points = drawing1.list();
        assertEquals(2, points.size());

        assertTrue(points.contains(PointCharacter.of(point1, c1)));
        assertTrue(points.contains(PointCharacter.of(point2, c2)));
    }

    @Test
    public void canReturnEmpty() {
        assertTrue(new MapDrawing().isEmpty(Point.of(1, 1)));
    }

    @Test
    public void canReturnFalseEmpty() {
        final Point point1 = Point.of(1, 1);
        final String c1 = "X";
        final MapDrawing mapDrawing = new MapDrawing();
        mapDrawing.add(PointCharacter.of(point1, c1));
        assertFalse(mapDrawing.isEmpty(point1));
    }

    @Test
    public void canCopy() {
        final Point point = Point.of(1, 1);
        final String c = "X";

        final MapDrawing drawing = new MapDrawing();
        drawing.add(PointCharacter.of(point, c));

        final MapDrawing drawing2 = drawing.copy();

        assertNotEquals(drawing, drawing2);
        assertEquals(drawing.get(point), drawing2.get(point));
    }
}
