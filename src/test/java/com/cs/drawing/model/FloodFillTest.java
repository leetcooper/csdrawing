package com.cs.drawing.model;

import com.cs.draw.model.Drawing;
import com.cs.draw.model.FloodFill;
import com.cs.draw.model.MapDrawing;
import com.cs.draw.model.Point;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class FloodFillTest {

    @Test
    public void canFloodFill() {
        final Drawing.PointCharacter p1 = Drawing.PointCharacter.of(Point.of(1, 0), "*");
        final Drawing.PointCharacter p2 = Drawing.PointCharacter.of(Point.of(4, 3), "*");
        final Drawing.PointCharacter p3 = Drawing.PointCharacter.of(Point.of(0, 0), "*");
        final Drawing.PointCharacter p4 = Drawing.PointCharacter.of(Point.of(4, 4), "*");
        final Drawing.PointCharacter p5 = Drawing.PointCharacter.of(Point.of(0, 1), "*");
        final Drawing.PointCharacter p6 = Drawing.PointCharacter.of(Point.of(3, 4), "*");
        final Drawing.PointCharacter p7 = Drawing.PointCharacter.of(Point.of(0, 2), "*");
        final Drawing.PointCharacter p8 = Drawing.PointCharacter.of(Point.of(2, 4), "*");
        final Drawing.PointCharacter p9 = Drawing.PointCharacter.of(Point.of(0, 3), "*");
        final Drawing.PointCharacter p10 = Drawing.PointCharacter.of(Point.of(1, 4), "*");
        final Drawing.PointCharacter p11 = Drawing.PointCharacter.of(Point.of(0, 4), "*");
        final Drawing.PointCharacter p12 = Drawing.PointCharacter.of(Point.of(4, 0), "*");
        final Drawing.PointCharacter p13 = Drawing.PointCharacter.of(Point.of(3, 0), "*");
        final Drawing.PointCharacter p14 = Drawing.PointCharacter.of(Point.of(4, 1), "*");
        final Drawing.PointCharacter p15 = Drawing.PointCharacter.of(Point.of(2, 0), "*");
        final Drawing.PointCharacter p16 = Drawing.PointCharacter.of(Point.of(4, 2), "*");

        List<Drawing.PointCharacter> points =
                List.of(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16);

        final MapDrawing mapDrawing = new MapDrawing();
        points.forEach(p -> mapDrawing.add(p));

        final FloodFill fill = FloodFill.of(Point.of(1, 1), "o")
                .withDrawingSnapshot(mapDrawing);

        final Drawing drawing = fill.draw();

        assertEquals(9, drawing.list().size());

        assertAtPoint(Point.of(1, 1), drawing, "o");
        assertAtPoint(Point.of(2, 1), drawing, "o");
        assertAtPoint(Point.of(3, 1), drawing, "o");
        assertAtPoint(Point.of(1, 2), drawing, "o");
        assertAtPoint(Point.of(2, 2), drawing, "o");
        assertAtPoint(Point.of(3, 2), drawing, "o");
        assertAtPoint(Point.of(1, 3), drawing, "o");
        assertAtPoint(Point.of(2, 3), drawing, "o");
        assertAtPoint(Point.of(3, 3), drawing, "o");
    }

    private void assertAtPoint(Point point, Drawing drawing, String c) {
        assertEquals(c, drawing.get(point).get().getCharacter());
    }
}
