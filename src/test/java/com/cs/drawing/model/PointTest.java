package com.cs.drawing.model;

import com.cs.draw.model.Point;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PointTest {
    @Test
    public void canEqualsHandleNull() {
        assertFalse(Point.of(0, 0).equals(null));
    }

    @Test
    public void canEqualsHandleDifferentType() {
        assertFalse(Point.of(0, 0).equals(new Object()));
    }

    @Test
    public void canEqualsHandleItSelf() {
        final Point point = Point.of(0, 0);
        assertTrue(point.equals(point));
    }

    @Test
    public void canEqualsMatchPoints() {
        final Point point1 = Point.of(0, 0);
        final Point point2 = Point.of(0, 0);
        assertTrue(point1.equals(point2));
    }

    @Test
    public void canEqualsNotMatchPointY() {
        final Point point1 = Point.of(0, 0);
        final Point point2 = Point.of(0, 1);
        assertFalse(point1.equals(point2));
    }

    @Test
    public void canEqualsNotMatchPointX() {
        final Point point1 = Point.of(0, 0);
        final Point point2 = Point.of(1, 0);
        assertFalse(point1.equals(point2));
    }

}
