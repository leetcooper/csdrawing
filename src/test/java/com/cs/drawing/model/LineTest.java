package com.cs.drawing.model;

import com.cs.draw.model.Drawing;
import com.cs.draw.model.Line;
import com.cs.draw.model.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LineTest {
    @Test
    public void canDrawHorizonalLine() {
        final Point point1 = Point.of(0, 0);
        final Point point2 = Point.of(0, 1);
        final Point point3 = Point.of(0, 2);

        Line line = Line.of(point1, point3);
        Drawing drawing = line.draw();
        assertEquals(3, drawing.list().size());
        assertTrue(drawing.get(point1).isPresent());
        assertTrue(drawing.get(point2).isPresent());
        assertTrue(drawing.get(point3).isPresent());

        assertStarAtPoint(point1, drawing);
        assertStarAtPoint(point2, drawing);
        assertStarAtPoint(point3, drawing);
    }

    @Test
    public void canDrawHorizonalLineReverse() {
        final Point point1 = Point.of(0, 0);
        final Point point2 = Point.of(0, 1);
        final Point point3 = Point.of(0, 2);

        Line line = Line.of(point3, point1);
        Drawing drawing = line.draw();
        assertEquals(3, drawing.list().size());
        assertTrue(drawing.get(point1).isPresent());
        assertTrue(drawing.get(point2).isPresent());
        assertTrue(drawing.get(point3).isPresent());

        assertStarAtPoint(point1, drawing);
        assertStarAtPoint(point2, drawing);
        assertStarAtPoint(point3, drawing);
    }

    @Test
    public void canDrawVerticalLine() {
        final Point point1 = Point.of(1, 0);
        final Point point2 = Point.of(2, 0);
        final Point point3 = Point.of(3, 0);

        Line line = Line.of(point1, point3);
        Drawing drawing = line.draw();
        assertEquals(3, drawing.list().size());
        assertTrue(drawing.get(point1).isPresent());
        assertTrue(drawing.get(point2).isPresent());
        assertTrue(drawing.get(point3).isPresent());

        assertStarAtPoint(point1, drawing);
        assertStarAtPoint(point2, drawing);
        assertStarAtPoint(point3, drawing);
    }

    @Test
    public void canDrawVerticalLineReverse() {
        final Point point1 = Point.of(3, 0);
        final Point point2 = Point.of(2, 0);
        final Point point3 = Point.of(1, 0);

        Line line = Line.of(point1, point3);
        Drawing drawing = line.draw();
        assertEquals(3, drawing.list().size());
        assertTrue(drawing.get(point1).isPresent());
        assertTrue(drawing.get(point2).isPresent());
        assertTrue(drawing.get(point3).isPresent());

        assertStarAtPoint(point1, drawing);
        assertStarAtPoint(point2, drawing);
        assertStarAtPoint(point3, drawing);
    }

    @Test
    public void canDrawEmptyLineWhenAcross() {
        final Point point1 = Point.of(0, 0);
        final Point point2 = Point.of(2, 2);

        Line line = Line.of(point1, point2);
        Drawing drawing = line.draw();
        assertEquals(0, drawing.list().size());
    }

    private void assertStarAtPoint(Point point, Drawing drawing) {
        assertEquals("*", drawing.get(point).get().getCharacter());
    }
}
