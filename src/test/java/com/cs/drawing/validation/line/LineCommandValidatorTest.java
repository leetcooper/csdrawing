package com.cs.drawing.validation.line;

import com.cs.draw.model.Line;
import com.cs.draw.model.Point;
import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.line.LineCommandValidator;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class LineCommandValidatorTest {
    @Test
    public void canReturnLineErrorIfAcross(){
        LineCommandValidator validator = new LineCommandValidator();
        assertTrue(validator.validate
                (Line.of(Point.of(0,0), Point.of(1,1)))
                .containsError(
                        ApplicationError
                                .of("LP",
                                        "Only horizontal and vertical line supported")));
    }

    @Test
    public void noLineErrorIfVertical(){
        LineCommandValidator validator = new LineCommandValidator();
        assertFalse(validator.validate
                (Line.of(Point.of(0,1), Point.of(1,1))).hasErrors());
    }

    @Test
    public void noLineErrorIfHorzontal(){
        LineCommandValidator validator = new LineCommandValidator();
        assertFalse(validator.validate
                (Line.of(Point.of(1,0), Point.of(1,1))).hasErrors());
    }
}
