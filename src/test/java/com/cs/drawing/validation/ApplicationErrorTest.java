package com.cs.drawing.validation;

import com.cs.draw.validation.ApplicationError;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ApplicationErrorTest {
    @Test
    public void canEqualsHandleNull(){
        assertFalse(ApplicationError.of("A","A").equals(null));
    }

    @Test
    public void canEqualsHandleDifferentType(){
        assertFalse(ApplicationError.of("A","A").equals(new Object()));
    }

    @Test
    public void canEqualsHandleItSelf(){
        final ApplicationError error = ApplicationError.of("A", "A");
        assertTrue(error.equals(error));
    }

    @Test
    public void canEqualsHandleMismatchCode(){
        final ApplicationError error1 = ApplicationError.of("A", "A");
        final ApplicationError error2 = ApplicationError.of("C", "A");
        assertFalse(error1.equals(error2));
    }

    @Test
    public void canEqualsHandleMismatchMessage(){
        final ApplicationError error1 = ApplicationError.of("A", "A");
        final ApplicationError error2 = ApplicationError.of("A", "C");
        assertFalse(error1.equals(error2));
    }


}
