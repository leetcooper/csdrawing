package com.cs.drawing.validation.rectangle;

import com.cs.draw.model.Dimension;
import com.cs.draw.model.Line;
import com.cs.draw.model.Point;
import com.cs.draw.model.Rectangle;
import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.line.LineCommandValidator;
import com.cs.draw.validation.rectangle.RectangleCommandValidator;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Ignore
public class RectangleCommandValidatorTest {
    public static final String FORMAT_ERROR =
            "Rectangle invalid expecting [R TLX TLY BRX BRY] \" +\n" +
                    "                    \"example: R 10 20 20 30\"";

    @Test
    public void canReturnRetangleDimensionErrorForZero(){
        RectangleCommandValidator validator = new RectangleCommandValidator();
        assertTrue(validator.validate(Rectangle.of(Point.of(0, 0), Dimension.of(0, 0),
                Rectangle.RectangleCharacters.of("*", "|")))
                .containsError(ApplicationError.of("NR", FORMAT_ERROR))
        );
    }

    @Test
    public void canReturnRetangleDimensionErrorForNegativeWidthAndHeight(){
        RectangleCommandValidator validator = new RectangleCommandValidator();
        assertTrue(validator.validate(Rectangle.of(Point.of(0, 0), Dimension.of(-1, -3),
                Rectangle.RectangleCharacters.of("*", "|")))
                .containsError(ApplicationError.of("NR", FORMAT_ERROR))
        );
    }

    @Test
    public void canReturnRetangleDimensionErrorForNegativeWidth(){
        RectangleCommandValidator validator = new RectangleCommandValidator();
        assertTrue(validator.validate(Rectangle.of(Point.of(0, 0), Dimension.of(-1, 3),
                Rectangle.RectangleCharacters.of("*", "|")))
                .containsError(ApplicationError.of("NR", FORMAT_ERROR))
        );
    }

    @Test
    public void canReturnRetangleDimensionErrorForNegativeHeight(){
        RectangleCommandValidator validator = new RectangleCommandValidator();
        assertTrue(validator.validate(Rectangle.of(Point.of(0, 0), Dimension.of(3, -3),
                Rectangle.RectangleCharacters.of("*", "|")))
                .containsError(ApplicationError.of("NR", FORMAT_ERROR))
        );
    }

}
