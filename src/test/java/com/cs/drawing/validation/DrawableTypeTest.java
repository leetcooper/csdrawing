package com.cs.drawing.validation;

import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.ApplicationException;
import com.cs.draw.validation.DrawableType;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
public class DrawableTypeTest {
    @Test
    public void canHandleBlankCommand(){
        try {
            DrawableType.typeOfCommand("");
        }
        catch(ApplicationException ae){
            assertTrue(ae.getErrors()
                    .containsError(ApplicationError
                            .of("UC","Unsupported command: ")));
        }
    }
}
