package com.cs.drawing.validation;

import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.ApplicationErrors;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class ApplicationErrorsTest {
    @Test
    public void canHandleMultipleErrors(){
        ApplicationErrors errors = ApplicationErrors
                .builder()
                .addError(ApplicationError.of("A", "A"))
                .addError(ApplicationError.of("B", "B"))
                .build();

        assertTrue(errors.containsError(ApplicationError.of("B", "B")));
    }

    @Test
    public void canHandleNoErrors(){
        ApplicationErrors errors = ApplicationErrors
                .builder()
                .addError(ApplicationError.of("A", "A"))
                .addError(ApplicationError.of("B", "B"))
                .build();

        assertFalse(errors.containsError(ApplicationError.of("C", "C")));
    }
}
