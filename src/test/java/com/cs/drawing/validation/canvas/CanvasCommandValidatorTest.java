package com.cs.drawing.validation.canvas;

import com.cs.draw.model.Canvas;
import com.cs.draw.model.Dimension;
import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.canvas.CanvasCommandValidator;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class CanvasCommandValidatorTest {
    @Test
    public void canReturnMaxError(){
        CanvasCommandValidator validator = new CanvasCommandValidator();
        assertTrue(validator.validate
                (Canvas.of(Dimension.of(1001, 1000)))
                .containsError(
                        ApplicationError
                                .of("MC",
                                        "Maximum Canvas Size of [1000000] Exceeded")));
    }

    @Test
    public void doesNotReturnMaxError(){
        CanvasCommandValidator validator = new CanvasCommandValidator();
        assertFalse(validator.validate
                (Canvas.of(Dimension.of(1000, 1000)))
                .hasErrors());
    }
}
