package com.cs.drawing;

import com.cs.draw.Main;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.nio.file.Paths;

import static java.nio.file.Files.readAllBytes;
import static org.junit.Assert.assertEquals;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

public class DrawEndToEndTest {
    public static final String QUIT = "Q";
    @Rule
    public final SystemErrRule stdErr = new SystemErrRule().enableLog();
    @Rule
    public final SystemOutRule stdOut = new SystemOutRule().enableLog();
    @Rule
    public final TextFromStandardInputStream stdIn = emptyStandardInputStream();

    @Test
    public void canQuit(){
        givenTheseCommands(QUIT);
        whenUsingDraw();
        thenExpect("canQuit.txt");
    }

    @Test
    public void canDrawCanvas(){
        givenTheseCommands("C 20 4", QUIT);
        whenUsingDraw();
        thenExpect("canDrawCanvas.txt");
    }

    @Test
    public void canDrawLine(){
        givenTheseCommands(
                "C 10 10",
                QUIT);

        whenUsingDraw();
        thenExpect("canDrawLine.txt");
    }

    @Test
    public void canDrawHorizonalAndVerticalLine(){
        givenTheseCommands(
                "C 20 4",
                "L 1 2 6 2",
                "L 6 3 6 4",
                QUIT);

        whenUsingDraw();
        thenExpect("canDrawHorizonalAndVerticalLine.txt");
    }

    @Test
    public void canDrawLinesAndRectangle(){
        givenTheseCommands(
                "C 20 4",
                "L 1 2 6 2",
                "L 6 3 6 4",
                "R 14 1 18 3",
                QUIT);

        whenUsingDraw();
        thenExpect("canDrawLinesAndRectangle.txt");
    }

    @Test
    public void canDrawRectangle(){
        givenTheseCommands(
                "C 20 4",
                "R 18 3 14 1",
                QUIT);

        whenUsingDraw();
        thenExpect("canDrawRectangle.txt");
    }

    @Test
    public void canBucketFill(){
        givenTheseCommands(
                "C 20 4",
                "L 1 2 6 2",
                "L 6 3 6 4",
                "R 14 1 18 3",
                "B 10 3 o",
                "B 2 2 w",
                QUIT);

        whenUsingDraw();
        thenExpect("canBucketFill.txt");
    }

    @Test
    public void canUndo(){
        givenTheseCommands(
                "C 20 4",
                "L 1 2 6 2",
                "L 6 3 6 4",
                "R 14 1 18 3",
                "B 10 3 o",
                "U",
                QUIT);

        whenUsingDraw();
        thenExpect("canUndo.txt");
    }

    @Test
    public void canShowLineError(){
        givenTheseCommands(
                "L 3 4 f",
                QUIT);

        whenUsingDraw();
        thenExpectError("canShowLineError.txt");
    }

    @Test
    public void canShowLineErrorWithCanvas(){
        givenTheseCommands(
                "C 20 4",
                "L 3 4 f",
                QUIT);

        whenUsingDraw();
        thenExpectError("canShowLineError.txt");
    }

    @Test
    public void canHandleUnknownError(){
        givenTheseCommands(
                "X ",
                QUIT);

        whenUsingDraw();
        thenExpectError("canHandleUnknownError.txt");
    }

    @Test
    public void canHandleMultipleUnknownError(){
        givenTheseCommands(
                "X ",
                "X ",
                QUIT);

        whenUsingDraw();
        thenExpectError("canHandleMultipleUnknownError.txt");
    }

    @Test
    public void canHandleUnknownErrorAfterFirstCommand(){
        givenTheseCommands(
                "C 20 4",
                "X ",
                QUIT);

        whenUsingDraw();
        thenExpectError("canHandleUnknownError.txt");
    }

    @Test
    public void canDrawDefaultCanvasWithLine(){
        givenTheseCommands(
                "L 1 2 6 2",
                QUIT);

        whenUsingDraw();
        thenExpect("canDrawDefaultCanvasWithLine.txt");
    }

    @Test
    public void canNotDrawOverBorder(){
        givenTheseCommands(
                "C 20 4",
                "L 0 2 6 2",
                QUIT);

        whenUsingDraw();
        thenExpect("canNotDrawOverBorder.txt");
    }

    @Test
    public void canResetCanvas(){
        givenTheseCommands(
                "C 20 4",
                "L 1 2 6 2",
                "C 20 5",
                "L 1 2 6 2",
                QUIT);

        whenUsingDraw();
        thenExpect("canResetCanvas.txt");
    }

    private void thenExpect(final String fileOutput){
        assertEquals(expectedConsoleOuput(fileOutput), stdOut.getLog());
    }

    private void thenExpectError(final String fileOutput){
        assertEquals(expectedConsoleOuput(fileOutput), stdErr.getLog());
    }

    private void givenTheseCommands(final String... commands){
        stdIn.provideLines(commands);
    }

    private void whenUsingDraw(){
        Main.main(new String[]{});
    }

    private String expectedConsoleOuput(final String file) {
        try {
            return new String(readAllBytes(Paths.get(getClass().getClassLoader().getResource(file).toURI())));
        }
        catch(Throwable e){
            throw new RuntimeException(e);
        }
    }
}
