package com.cs.draw.output;

public interface ConsoleOutput {
    void print(final String line);
}
