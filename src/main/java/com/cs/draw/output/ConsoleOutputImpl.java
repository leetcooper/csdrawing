package com.cs.draw.output;
import static java.lang.System.out;
public class ConsoleOutputImpl implements ConsoleOutput {

    @Override
    public void print(final String line) {
        out.print(line);
    }
}
