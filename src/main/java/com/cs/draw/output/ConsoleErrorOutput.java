package com.cs.draw.output;

public interface ConsoleErrorOutput {
    void print(final String line);
}
