package com.cs.draw.output;
import static java.lang.System.err;

public class ConsoleErrorOutputImpl implements ConsoleErrorOutput {

    @Override
    public void print(final String line) {
        err.print(line);
    }
}
