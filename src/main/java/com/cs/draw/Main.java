package com.cs.draw;

import com.cs.draw.drawing.Drawing;
import com.google.inject.Guice;
import com.google.inject.Injector;

import static com.cs.draw.GuiceBundle.getInstance;

public class Main {
    private Main(){

    }

    public static void main(final String cmd[]){
        final Injector injector = Guice.createInjector(getInstance());

        final Drawing consoleDrawing = injector.getInstance(Drawing.class);

        consoleDrawing.start();
    }
}
