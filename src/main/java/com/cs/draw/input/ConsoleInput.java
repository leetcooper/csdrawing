package com.cs.draw.input;

public interface ConsoleInput {
    String nextLine();
}
