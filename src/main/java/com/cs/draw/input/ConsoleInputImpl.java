package com.cs.draw.input;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class ConsoleInputImpl implements ConsoleInput {

    final AtomicInteger count = new AtomicInteger(0);
    private Scanner scanner = new Scanner(System.in);

    @Override
    public String nextLine() {
        final String line = scanner.nextLine();
        count.incrementAndGet();
        return line.trim();
    }
}
