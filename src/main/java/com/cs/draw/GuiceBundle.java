package com.cs.draw;

import com.cs.draw.drawing.ConsoleDrawingImpl;
import com.cs.draw.drawing.Drawing;
import com.cs.draw.input.ConsoleInput;
import com.cs.draw.input.ConsoleInputImpl;
import com.cs.draw.output.ConsoleErrorOutput;
import com.cs.draw.output.ConsoleErrorOutputImpl;
import com.cs.draw.output.ConsoleOutput;
import com.cs.draw.output.ConsoleOutputImpl;
import com.google.inject.AbstractModule;

public class GuiceBundle extends AbstractModule {

    private GuiceBundle(){
    }

    @Override
    protected void configure() {
        bind(Drawing.class).to(ConsoleDrawingImpl.class);
        bind(ConsoleInput.class).to(ConsoleInputImpl.class);
        bind(ConsoleOutput.class).to(ConsoleOutputImpl.class);
        bind(ConsoleErrorOutput.class).to(ConsoleErrorOutputImpl.class);
    }

    private static class LazyHolder {
        static final GuiceBundle INSTANCE = new GuiceBundle();
    }

    public static GuiceBundle getInstance(){
        return LazyHolder.INSTANCE;
    }
}
