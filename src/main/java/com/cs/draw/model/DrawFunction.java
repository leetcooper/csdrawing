package com.cs.draw.model;

public abstract class DrawFunction<T extends Drawable> implements Drawable{
    public abstract T withDrawingSnapshot(Drawing snapshot);
}
