package com.cs.draw.model;

import com.cs.draw.model.Drawing.PointCharacter;

import java.util.Objects;
import java.util.stream.IntStream;

public class Line extends Shape{

    private final Point from;
    private final Point to;
    private final LineDirection direction;

    private Line(final Point from, final Point to){
        Objects.requireNonNull(from);
        Objects.requireNonNull(to);
        this.from = from;
        this.to = to;
        this.direction = calculateDirection();
    }

    private LineDirection calculateDirection() {
        if(from.isOnSameYAxis(to)){
            return LineDirection.HORIZONTAL;
        }
        if(from.isOnSameXAxis(to)){
            return LineDirection.VERTICAL;
        }
        return LineDirection.ACROSS;
    }

    public static Line of(final Point from, final Point to){
        return new Line(from, to);
    }

    @Override
    public Drawing draw() {
        if(direction.isHorizontal()){
            return drawHorizontalLine();
        }
        if(direction.isVertical()){
            return drawVerticalLine();
        }
        return DrawingFactory.getDefault();
    }

    private Drawing drawVerticalLine() {
        final Drawing drawing = DrawingFactory.getDefault();
        int lowest = getLowestVerticalPosition();
        int highest = getHighestVerticalPosition(lowest);
        IntStream.rangeClosed(lowest, highest).forEach(i ->
            drawing
                .add(
                    PointCharacter.of(Point.of(from.getX(), i), "*")));
        return drawing;
    }

    private Drawing drawHorizontalLine() {
        final Drawing drawing = DrawingFactory.getDefault();
        int lowest = getLowestHorizontalPosition();
        int highest = getHighestHorizontalPosition(lowest);
        IntStream.rangeClosed(lowest, highest).forEach(i ->
            drawing
                .add(
                    PointCharacter.of(Point.of(i, from.getY()), "*")));
        return drawing;
    }

    private int getHighestVerticalPosition(int lowest) {
        return lowest == from.getY() ? to.getY() : from.getY();
    }

    private int getLowestVerticalPosition() {
        return from.getY() < to.getY() ? from.getY() : to.getY();
    }

    private int getHighestHorizontalPosition(int lowest) {
        return lowest == from.getX() ? to.getX() : from.getX();
    }

    private int getLowestHorizontalPosition() {
        return from.getX() < to.getX() ? from.getX() : to.getX();
    }

    public boolean isAcross() {
        return direction.isAcross();
    }

    public enum LineDirection{
        HORIZONTAL,
        VERTICAL,
        ACROSS;

        public boolean isHorizontal() {
            return this == HORIZONTAL;
        }

        public boolean isVertical() {
            return this == VERTICAL;
        }

        public boolean isAcross() {
            return this == ACROSS;
        }
    }
}