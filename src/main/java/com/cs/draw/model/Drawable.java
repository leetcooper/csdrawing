package com.cs.draw.model;

public interface Drawable extends Command{
    Drawing draw();
}
