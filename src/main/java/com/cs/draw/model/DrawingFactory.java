package com.cs.draw.model;

public class DrawingFactory {
    private DrawingFactory(){

    }

    public static Drawing getDefault(){
        return new MapDrawing();
    }
}
