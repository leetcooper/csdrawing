package com.cs.draw.model;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Map implementation of a drawing. A Drawing in this context means a group of positions and characters.
 * The map is used as a convenient way to look up a character at a specific position
 * The contents on the map is not intended to hold white space, only human visible characters.
 */
public class MapDrawing implements Drawing{

    private Map<Point, PointCharacter> drawingPoints = new ConcurrentHashMap<>();

    public Optional<PointCharacter> get(final Point point){
        Objects.requireNonNull(point);
        return Optional.ofNullable(
                drawingPoints
                    .getOrDefault(point, null));
    }

    public void add(final PointCharacter pointCharacter){
        drawingPoints.put(pointCharacter.getPoint(), pointCharacter);
    }

    public boolean isEmpty(Point p) {
        return !get(p).isPresent();
    }

    public MapDrawing copy(){
        final MapDrawing drawing = new MapDrawing();
        drawingPoints
                .keySet()
                .stream()
                .map(k -> drawingPoints.get(k))
                .forEach(pc -> drawing.add(pc));
        return drawing;
    }

    public void addAll(final Drawing drawing) {
        drawing.list().forEach(this::add);
    }

    @Override
    public Collection<PointCharacter> list() {
        return this.drawingPoints.values();
    }
}
