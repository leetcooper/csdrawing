package com.cs.draw.model;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

public interface Drawing {

    Optional<PointCharacter> get(final Point point);
    void add(final PointCharacter pointCharacter);
    boolean isEmpty(Point p);
    Drawing copy();
    void addAll(final Drawing drawing);
    Collection<PointCharacter> list();

    class PointCharacter{
        private Point point;
        private String character;

        private PointCharacter(Point point, String character) {
            Objects.requireNonNull(point);
            Objects.requireNonNull(character);
            this.point = point;
            this.character = character;
        }

        public static PointCharacter of(Point point, String character){
            return new PointCharacter(point, character);
        }

        public Point getPoint() {
            return point;
        }

        public String getCharacter() {
            return character;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PointCharacter that = (PointCharacter) o;
            return Objects.equals(point, that.point) &&
                    Objects.equals(character, that.character);
        }

        @Override
        public int hashCode() {
            return Objects.hash(point, character);
        }
    }
}
