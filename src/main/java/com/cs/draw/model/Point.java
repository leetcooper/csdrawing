package com.cs.draw.model;

import java.util.Objects;

public class Point {
    private int x;
    private int y;

    private Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static Point of(int x, int y){
        return new Point(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public boolean isOnSameYAxis(final Point p) {
        return y == p.y;
    }

    public boolean isOnSameXAxis(final Point p) {
        return x == p.x;
    }

    public Point above() {
        return Point.of(x, y-1);
    }

    public Point below() {
        return Point.of(x, y+1);
    }

    public Point left() {
        return Point.of(x-1, y);
    }

    public Point right() {
        return Point.of(x+1, y);
    }
}
