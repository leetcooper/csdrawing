package com.cs.draw.model;

/**
 * Todo: add unit test
 */
public class Dimension {
    private int w;
    private int h;

    private Dimension(int width, int height){
        this.w = width;
        this.h = height;
    }

    public static Dimension of(int w, int h){
        return new Dimension(w, h);
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public Dimension expandBy(int i) {
        return Dimension.of(w+i, h+i);
    }

    public boolean greaterThen(Dimension max) {
        return this.size() > max.size();
    }

    public int size() {
        return w*h;
    }

}
