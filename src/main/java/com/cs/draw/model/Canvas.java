package com.cs.draw.model;

import com.cs.draw.model.Drawing.PointCharacter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

import static java.lang.System.lineSeparator;

/**
 * Canvas encapsulates all drawable objects and provides convenient renderCanvas
 * which executes callbacks to a functional interface implementation
 * so the concrete rendering implementation is decoupled from the composite canvas
 * @link renderCanvas
 *
 * Todo: Add unit test
 */
public class Canvas extends Rectangle {
    public static final String SPACE = " ";
    final List<Drawable> drawables = new CopyOnWriteArrayList<>();

    private Canvas(final Dimension dimension){
        super(Point.of(1, 1), dimension,
                RectangleCharacters.of("",""));
    }

    public static Canvas of(final Dimension dimension){
        return new Canvas(dimension);
    }

    public void addDrawable(final Drawable drawable) {
        drawables.add(drawable);
    }

    public void undo(){
        drawables.remove(drawables.size()-1);
    }

    @Override
    public Drawing draw(){
        return drawAndMergeEverything(getBorder());
    }

    public void renderCanvas(final CanvasConsumer consumer){
        final Drawing drawings = draw();
        final Rectangle border = getBorder();
        border.applyRectangleGridTo(
            forEveryColumnRowSendCharacterToConsumer(consumer, drawings),
            forEveryNewRowSendLineBreakToConsumer(consumer)
        );
    }

    private Drawing drawAndMergeEverything(final Rectangle border) {
        final Drawing borderDrawing = border.draw();

        final List<Drawing> allDrawings =
                combine(borderDrawing, getDrawings(borderDrawing));

        return mergeDrawings(border, allDrawings);
    }

    private Drawing mergeDrawings(Rectangle border, List<Drawing> allDrawings) {
        final Drawing merge = DrawingFactory.getDefault();
        border.applyRectangleGridTo(p ->
                allDrawings.stream().forEach(s ->
                        s.get(p)
                            .ifPresent(c -> merge
                                .add(PointCharacter.of(p, c.getCharacter())))
                ), onNewRowDoNothing()
        );
        return merge;
    }

    private NewRowFunction forEveryNewRowSendLineBreakToConsumer(CanvasConsumer consumer) {
        return r -> consumer.accept(lineSeparator());
    }

    private NewColumnFunction forEveryColumnRowSendCharacterToConsumer(
            final CanvasConsumer consumer,
            final Drawing characterWithPositions) {

        return p ->{
                final String c = getChar(characterWithPositions, p);
                consumer.accept(c);
        };
    }

    private Rectangle getBorder() {
        return Rectangle.of(Point.of(0, 0), dimension.expandBy(2),
                RectangleCharacters.of("*","|"));
    }

    private String getChar(Drawing characterWithPositions, Point p) {
        return characterWithPositions
                .get(p)
                .map(PointCharacter::getCharacter)
                .orElse(SPACE);
    }

    private List<Drawing> combine(Drawing borderDrawing, List<Drawing> drawings) {
        final List<Drawing> drawingsWithBorder = new ArrayList<>();
        drawingsWithBorder.addAll(drawings);
        drawingsWithBorder.add(borderDrawing);
        return drawingsWithBorder;
    }

    private List<Drawing> getDrawings(Drawing borderDrawing) {
        final List<Drawing> drawings = new ArrayList<>();
        final Drawing drawingSnapshot = borderDrawing.copy();
        for(final Drawable drawable : drawables){
            if(drawable instanceof DrawFunction){
                final Drawing functionDrawing = ((DrawFunction) drawable)
                    .withDrawingSnapshot(drawingSnapshot)
                    .draw();

                drawings.add(functionDrawing);
            }
            else{
                final Drawing drawing = drawable.draw();
                drawingSnapshot.addAll(drawing);
                drawings.add(drawing);
            }
        }
        return drawings;
    }

    private NewRowFunction onNewRowDoNothing() {
        return r -> Function.identity();
    }

    public interface CanvasConsumer{
        void accept(String c);
    }
}