package com.cs.draw.model;

import com.cs.draw.model.Drawing.PointCharacter;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public class Rectangle extends Shape{
    protected Point point;
    protected Dimension dimension;
    protected RectangleCharacters characters;

    public Rectangle(final Point point, final Dimension dimension, final RectangleCharacters characters) {
        Objects.requireNonNull(point);
        Objects.requireNonNull(dimension);
        Objects.requireNonNull(characters);
        this.point = point;
        this.dimension = dimension;
        this.characters = characters;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public static Rectangle of(final Point point, final Dimension dimension,
                               final RectangleCharacters properties) {
        return new Rectangle(point, dimension, properties);
    }

    @Override
    public Drawing draw() {
        final Drawing drawing = DrawingFactory.getDefault();
        applyRectangleGridTo(p ->
            forEveryColumnRowAddCharIfBorder(drawing, p),
            doNothingForEveryNewRow());
        return drawing;
    }

    private void forEveryColumnRowAddCharIfBorder(Drawing drawing, Point p) {
        calculateChar(p).ifPresent(c ->
            drawing.add(
                    PointCharacter.of(p, c))
        );
    }

    private NewRowFunction doNothingForEveryNewRow() {
        return r -> Function.identity();
    }

    protected Optional<String> calculateChar(Point p) {
        if(isEdge(p.getY(), point.getY(), dimension.getH())){
            return Optional.of(characters.getHorizontalCharacter());
        }
        if(isEdge(p.getX(), point.getX(), dimension.getW())){
            return Optional.of(characters.getVerticalCharacter());
        }
        return Optional.empty();
    }

    private boolean isEdge(int p1, int p2, int e) {
        return p1 == p2 || p1 == p2 + e - 1;
    }


    protected void applyRectangleGridTo(
            final NewColumnFunction newColumnFunction,
            final NewRowFunction newRowFunction) {
        int h = dimension.getH();
        int w = dimension.getW();
        for(int y = point.getY(); y < h+point.getY(); y++){
            for(int x = point.getX(); x < w+point.getX(); x++){
                newColumnFunction.apply(Point.of(x, y));
            }
            newRowFunction.apply(y);
        }
    }

    public interface NewColumnFunction {
        void apply(Point point);
    }

    public interface NewRowFunction {
        void apply(int row);
    }

    public static class RectangleCharacters{
        private final String horizontalCharacter;
        private final String verticalCharacter;

        private RectangleCharacters(String h, String v) {
            this.horizontalCharacter = h;
            this.verticalCharacter = v;
        }

        public static RectangleCharacters of(String h, String v){
            return new RectangleCharacters(h, v);
        }

        public String getHorizontalCharacter() {
            return horizontalCharacter;
        }

        public String getVerticalCharacter() {
            return verticalCharacter;
        }
    }
}
