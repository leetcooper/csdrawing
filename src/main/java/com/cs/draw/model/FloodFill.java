package com.cs.draw.model;

import com.cs.draw.model.Drawing.PointCharacter;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;

public class FloodFill extends DrawFunction<FloodFill>{

    private Point point;
    private String character;
    private Optional<Drawing> snapShotDrawing;


    public FloodFill(Point point, String character) {
        Objects.requireNonNull(point);
        Objects.requireNonNull(character);
        this.point = point;
        this.character = character;
    }

    private FloodFill(Point point, String character, Drawing existingPostions) {
        this(point, character);
        Objects.requireNonNull(existingPostions);
        this.snapShotDrawing = Optional.ofNullable(existingPostions);
    }

    public static FloodFill of(final Point p, String c) {
        return new FloodFill(p, c);
    }

    public FloodFill withDrawingSnapshot(Drawing existingDrawings) {
        return new FloodFill(point,character,existingDrawings);
    }

    @Override
    public Drawing draw() {
        return snapShotDrawing
            .map(ex -> floodFill())
            .orElse(DrawingFactory.getDefault());
    }

    private Drawing floodFill() {
        final Drawing drawing = snapShotDrawing.get().copy();
        final Drawing layer = DrawingFactory.getDefault();
        final Queue<Point> q = new LinkedList<>();
        final PointCharacter pcToMatch =
                drawing.get(point)
                .orElse(PointCharacter.of(point, ""));

        q.add(point);

        while (!q.isEmpty()) {
            final Point p = q.remove();

            if(isMatch(pcToMatch, drawing.get(p).orElse(PointCharacter.of(point, "")))) {
                final PointCharacter pc = PointCharacter.of(p, character);
                drawing.add(pc);
                layer.add(pc);
                q.add(p.above());
                q.add(p.below());
                q.add(p.left());
                q.add(p.right());
            }
        }

        return layer;
    }

    private boolean isMatch(PointCharacter p1, PointCharacter p2) {
        return p1.getCharacter().equals(p2.getCharacter());
    }
}
