package com.cs.draw.drawing;

import com.cs.draw.model.Canvas;
import com.cs.draw.model.Command;
import com.cs.draw.model.Drawable;
import com.cs.draw.model.Undo;
import com.cs.draw.validation.*;

import java.util.Objects;
import java.util.Optional;

public class CommandExecutor<T extends Command> {
    private final String command;
    private final CommandValidator commandValidator;
    private final CommandParser<T> commandParser;
    private Optional<Canvas.CanvasConsumer> canvasConsumer;
    private Optional<Canvas> canvas = Optional.empty();

    private CommandExecutor(
            String command,
            CommandValidator commandValidator,
            CommandParser<T> commandParser) {
        Objects.requireNonNull(commandParser);
        Objects.requireNonNull(commandValidator);
        Objects.requireNonNull(command);
        this.command = command;
        this.commandValidator = commandValidator;
        this.commandParser = commandParser;
    }

    public static <T extends Command> CommandExecutor<T> getInstance(CommandValidatorParserProvider type, String command){
        return new CommandExecutor<>(command, type.getCommandValidator(), type.getCommandParser());
    }

    public CommandExecutor onCanvas(Canvas canvas){
        this.canvas = Optional.ofNullable(canvas);
        return this;
    }

    public CommandExecutor onValidCommand(Canvas.CanvasConsumer canvasConsumer){
        this.canvasConsumer = Optional.ofNullable(canvasConsumer);
        return this;
    }

    public Command orElse(OnError onError) throws ApplicationException {
        Objects.requireNonNull(onError);

        final T command = commandParser.parse(this.command);
        if(command instanceof Canvas)
            canvas = Optional.ofNullable((Canvas) command);

        ApplicationErrors errors = commandValidator.validate(command);

        if(errors.hasErrors()){
            onError.onError(errors);
        }
        else{
            canvas.ifPresent(c -> {
                if(command != c && command instanceof Drawable) {
                    c.addDrawable((Drawable) command);
                }
                else if(command instanceof Undo){
                    c.undo();
                }
                canvasConsumer.ifPresent(cc -> c.renderCanvas(cc));
            });
        }
        return command;
    }

    public interface OnError{
        void onError(ApplicationErrors errors);
    }
}
