package com.cs.draw.drawing;

import com.cs.draw.input.ConsoleInput;
import com.cs.draw.model.Canvas;
import com.cs.draw.model.Command;
import com.cs.draw.model.Dimension;
import com.cs.draw.model.Drawable;
import com.cs.draw.output.ConsoleErrorOutput;
import com.cs.draw.output.ConsoleOutput;
import com.cs.draw.validation.ApplicationErrors;
import com.cs.draw.validation.ApplicationException;

import javax.inject.Inject;
import java.util.Objects;
import java.util.Optional;

import static com.cs.draw.validation.DrawableType.typeOfCommand;
import static java.util.Optional.of;

/**
 * Responsible for user flow and console interaction.
 */
public class ConsoleDrawingImpl implements Drawing {
    private final ConsoleOutput output;
    private final ConsoleInput input;
    private final ConsoleErrorOutput error;

    @Inject
    public ConsoleDrawingImpl(
            final ConsoleOutput output,
            final ConsoleInput input,
            final ConsoleErrorOutput error){
        Objects.requireNonNull(output);
        Objects.requireNonNull(input);
        Objects.requireNonNull(error);

        this.output = output;
        this.input = input;
        this.error = error;
    }

    @Override
    public void start() {

        pleaseEnterCommand();
        final CanvasOrQuit canvasOrQuit = firstCommand(nextCommand());
        if(canvasOrQuit.isQuit()) return;
        Canvas canvas = canvasOrQuit.canvas.get();

        while (true) {
            try {
                pleaseEnterCommand();

                final String command = nextCommand();
                if (shouldQuit(command)) break;

                final Command drawable = processCommand(command, of(canvas));

                if(drawable instanceof Canvas)
                    canvas = (Canvas)drawable;
            }
            catch (ApplicationException ae){
                this.onError(ae.getErrors());
            }
        }
    }

    private String nextCommand() {
        return input.nextLine().trim();
    }

    private Command processCommand(String command, Optional<Canvas> canvas) {
        final CommandExecutor commandExecutor =
                CommandExecutor.getInstance(typeOfCommand(command), command);

        if(canvas.isPresent()){
            return commandExecutor
                .onCanvas(canvas.get())
                .onValidCommand(output::print)
                .orElse(this::onError);
        }
        else{
            return commandExecutor
                    .onValidCommand(output::print)
                    .orElse(this::onError);
        }
    }

    private CanvasOrQuit firstCommand(String firstCommand) {
        if (shouldQuit(firstCommand)) return new CanvasOrQuit(Optional.empty());
        while(true) {
            try {
                final Command drawable = processCommand(firstCommand, Optional.empty());

                if (drawable instanceof Canvas) {
                    return CanvasOrQuit.of((Canvas) drawable);
                } else {
                    final Canvas canvas = Canvas.of(Dimension.of(20, 20));
                    processCommand(firstCommand,
                            of(canvas));
                    return CanvasOrQuit.of(canvas);
                }
            }
            catch (ApplicationException ae){
                this.onError(ae.getErrors());
            }

            firstCommand = input.nextLine();
            if (shouldQuit(firstCommand)) return CanvasOrQuit.quit();
        }
    }

    private void onError(ApplicationErrors ae) {
        ae.forEach(a -> error.print(a.getErrorMessage() + System.lineSeparator()));
    }

    private void pleaseEnterCommand() {
        output.print("enter command:");
        output.print(System.lineSeparator());
    }

    private boolean shouldQuit(String userInput) {
        return "Q".equals(userInput.substring(0, 1));
    }

    private static class CanvasOrQuit{
        private Optional<Canvas> canvas;

        public static CanvasOrQuit quit(){
            return new CanvasOrQuit(Optional.empty());
        }

        public static CanvasOrQuit of(Canvas canvas){
            return new CanvasOrQuit(Optional.of(canvas));
        }

        private CanvasOrQuit(Optional<Canvas> canvas){
            this.canvas = canvas;
        }

        public boolean isQuit(){
            return !canvas.isPresent();
        }
    }

}
