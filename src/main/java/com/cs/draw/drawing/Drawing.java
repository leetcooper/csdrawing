package com.cs.draw.drawing;

/**
 * Stating interface responsible for kick starting a program
 */
public interface Drawing {
    void start();
}
