package com.cs.draw.validation;

import com.cs.draw.validation.canvas.CanvasCommandParser;
import com.cs.draw.validation.canvas.CanvasCommandValidator;
import com.cs.draw.validation.floodfill.FloodFillCommandParser;
import com.cs.draw.validation.floodfill.FloodFillCommandValidator;
import com.cs.draw.validation.line.LineCommandParser;
import com.cs.draw.validation.line.LineCommandValidator;
import com.cs.draw.validation.rectangle.RectangleCommandParser;
import com.cs.draw.validation.rectangle.RectangleCommandValidator;

import java.util.Objects;

/**
 * Responsible for identifying type of command and groups validation and
 * parsing singletons for that drawable type
 */
/**
 * Todo: add unit test
 */
public enum DrawableType implements CommandValidatorParserProvider {
    C(new CanvasCommandValidator(),new CanvasCommandParser()),
    L(new LineCommandValidator(), new LineCommandParser()),
    R(new RectangleCommandValidator(), new RectangleCommandParser()),
    B(new FloodFillCommandValidator(), new FloodFillCommandParser()),
    U(new UndoCommandValidator(), new UndoCommandParser());

    private final CommandValidator commandValidator;
    private final CommandParser commandParser;

    DrawableType(CommandValidator commandValidator, CommandParser commandParser) {
        Objects.requireNonNull(commandValidator);
        Objects.requireNonNull(commandParser);
        this.commandValidator = commandValidator;
        this.commandParser = commandParser;
    }

    public CommandValidator getCommandValidator() {
        return commandValidator;
    }

    public CommandParser getCommandParser() {
        return commandParser;
    }

    public static final String UNSUPPORTED_COMMAND = "Unsupported command";

    public static DrawableType typeOfCommand(final String command){
        Objects.requireNonNull(command);
        if(command.length() < 1) {
            throw unsupportedCommandException(command);
        }

        final String firstLetter = firstLetter(command);

        try{
            return DrawableType.valueOf(firstLetter);
        }
        catch (Throwable e){
            throw unsupportedCommandException(command);
        }
    }

    private static ApplicationException unsupportedCommandException(String command) {
        return ApplicationException
                .withError(ApplicationError
                        .of("UC", UNSUPPORTED_COMMAND + ": " + command));
    }

    private static String firstLetter(String command) {
        return command.substring(0, 1);
    }
}
