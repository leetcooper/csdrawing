package com.cs.draw.validation;

import java.util.Objects;

public class ApplicationError {
    private String code;
    private String errorMessage;

    public ApplicationError(String code, String errorMessage) {
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public static ApplicationError of(final String code, final String systemMessage) {
        return new ApplicationError(code, systemMessage);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApplicationError that = (ApplicationError) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(errorMessage, that.errorMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, errorMessage);
    }
}
