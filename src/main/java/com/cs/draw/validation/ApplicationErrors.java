package com.cs.draw.validation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class ApplicationErrors {

    private List<ApplicationError> errors = new ArrayList<>();

    public static ApplicationErrors empty() {
        return new ApplicationErrorBuilder().build();
    }
    public static ApplicationErrors of(ApplicationError... error) {
        ApplicationErrorBuilder builder = new ApplicationErrorBuilder();
        List.of(error).forEach(builder::addError);
        return builder.build();
    }


    public List<ApplicationError> getErrors() {
        return errors;
    }

    public void forEach(Consumer<ApplicationError> consumer){
        errors.stream().forEach(consumer);
    }

    public boolean containsError(final ApplicationError e) {
        for (ApplicationError error : this.getErrors()) {
            if (e.equals(error)) {
                return true;
            }
        }
        return false;
    }

    public static ApplicationErrorBuilder builder(){
        return new ApplicationErrorBuilder();
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

    public static class ApplicationErrorBuilder {

        private ApplicationErrorBuilder(){

        }

        final Set<ApplicationError> errors = new HashSet<ApplicationError>();

        public ApplicationErrorBuilder addError(final ApplicationError e) {
            errors.add(e);
            return this;
        }

        public ApplicationErrors build() {
            final ApplicationErrors errorResponse = new ApplicationErrors();

            errors
                .stream()
                .forEach(e -> errorResponse.getErrors().add(e));

            return errorResponse;
        }
    }

}