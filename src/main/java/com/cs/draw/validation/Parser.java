package com.cs.draw.validation;

public interface Parser<T>  {
    T parse(final String command);
}
