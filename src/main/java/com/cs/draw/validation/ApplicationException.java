package com.cs.draw.validation;

import java.util.Objects;

/**
 * Todo: add unit test
 */
public class ApplicationException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private final ApplicationErrors errors;

    public ApplicationException(final ApplicationErrors errors) {
        Objects.requireNonNull(errors);
        this.errors = errors;
    }

    public ApplicationErrors getErrors() {
        return errors;
    }

    public static ApplicationException withError(ApplicationError error){
        return new ApplicationException(ApplicationErrors.of(error));
    }
}