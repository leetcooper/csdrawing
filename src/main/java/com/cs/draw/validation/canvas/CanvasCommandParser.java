package com.cs.draw.validation.canvas;

import com.cs.draw.model.Canvas;
import com.cs.draw.model.Dimension;
import com.cs.draw.validation.CommandParser;
/**
 * Todo: add unit test
 */
public class CanvasCommandParser extends CommandParser<Canvas> {

    private static final String FORMAT_ERROR = "Canvas invalid expecting [C W H] example: C 10 20";
    public static final String COMMAND_FORMAT = "C [0-9]+ [0-9]+";

    public CanvasCommandParser() {
        super(COMMAND_FORMAT, FORMAT_ERROR);
    }

    @Override
    public Canvas parse(final String command) {
            checkFormat(command);
        return Canvas.of(Dimension.of(getCanvasWidth(command)
                , getCanvasHeight(command)));
    }

    private int getCanvasWidth(final String command) {
        return Integer.parseInt(command.split(" ")[1]);
    }

    private int getCanvasHeight(final String command) {
        return Integer.parseInt(command.split(" ")[2]);
    }
}
