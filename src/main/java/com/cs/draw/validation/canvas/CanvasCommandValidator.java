package com.cs.draw.validation.canvas;

import com.cs.draw.model.Canvas;
import com.cs.draw.model.Dimension;
import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.ApplicationErrors;
import com.cs.draw.validation.CommandValidator;

import java.util.Objects;
import java.util.Optional;

public class CanvasCommandValidator extends CommandValidator<Canvas> {
    public final static Dimension MAX_CANVAS = Dimension.of(1000, 1000);

    @Override
    public ApplicationErrors validate(final Canvas canvas){
        Objects.requireNonNull(canvas);
        final ApplicationErrors.ApplicationErrorBuilder builder = ApplicationErrors.builder();
        final Optional<ApplicationError> maxError = checkMaxCanvasSize(canvas);
        maxError.ifPresent(builder::addError);
        return builder.build();
    }

    private Optional<ApplicationError> checkMaxCanvasSize(Canvas canvas) {
        return (canvas.getDimension().greaterThen(MAX_CANVAS)) ?
                Optional.of(ApplicationError
                        .of("MC",
                                String.format("Maximum Canvas Size of [%s] Exceeded"
                                        , MAX_CANVAS.size())))
                :
                Optional.empty();
    }
}


