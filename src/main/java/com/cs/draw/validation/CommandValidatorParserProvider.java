package com.cs.draw.validation;

public interface CommandValidatorParserProvider {
    CommandValidator getCommandValidator();
    CommandParser getCommandParser();
}
