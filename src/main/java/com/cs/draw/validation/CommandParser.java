package com.cs.draw.validation;

import com.cs.draw.model.Command;
import com.cs.draw.model.Drawable;

import java.util.Objects;

public abstract class CommandParser<T extends Command> implements Parser<T>{

    private final String FORMAT_ERROR;
    public final String COMMAND_FORMAT;

    public CommandParser(String COMMAND_FORMAT, String FORMAT_ERROR) {
        Objects.requireNonNull(COMMAND_FORMAT);
        Objects.requireNonNull(FORMAT_ERROR);
        this.FORMAT_ERROR = FORMAT_ERROR;
        this.COMMAND_FORMAT = COMMAND_FORMAT;
    }

    public void checkFormat(final String command){
        Objects.requireNonNull(command);
        if(!command.matches(COMMAND_FORMAT))
            throw new ApplicationException(
                    ApplicationErrors
                            .of(ApplicationError.of("FE", FORMAT_ERROR)));
    }

}
