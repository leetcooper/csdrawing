package com.cs.draw.validation;

import com.cs.draw.model.Undo;

public class UndoCommandValidator extends CommandValidator<Undo> {
    @Override
    public ApplicationErrors validate(Undo undo) {
        return ApplicationErrors.empty();
    }
}
