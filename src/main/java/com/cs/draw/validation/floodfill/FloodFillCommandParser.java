package com.cs.draw.validation.floodfill;

import com.cs.draw.model.FloodFill;
import com.cs.draw.model.Point;
import com.cs.draw.validation.CommandParser;
/**
 * Todo: add unit test
 */
public class FloodFillCommandParser extends CommandParser<FloodFill> {

    public FloodFillCommandParser() {
        super("B [0-9]+ [0-9]+ [^\\s]",
                "Bucket fill command invalid expecting [B X Y %S] example: B 5 7 o");
    }

    @Override
    public FloodFill parse(final String command) {
        super.checkFormat(command);

        final String[] split = command.split(" ");
        int x = Integer.parseInt(split[1]);
        int y = Integer.parseInt(split[2]);
        String c = split[3];

        return FloodFill.of(Point.of(x, y), c);
    }
}
