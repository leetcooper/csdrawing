package com.cs.draw.validation.floodfill;

import com.cs.draw.model.FloodFill;
import com.cs.draw.validation.ApplicationErrors;
import com.cs.draw.validation.CommandValidator;
/**
 * Todo: add unit test
 */
public class FloodFillCommandValidator extends CommandValidator<FloodFill> {
    @Override
    public ApplicationErrors validate(final FloodFill floodFill){
        return ApplicationErrors.empty();
    }
}


