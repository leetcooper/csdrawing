package com.cs.draw.validation.line;

import com.cs.draw.model.Line;
import com.cs.draw.model.Point;
import com.cs.draw.validation.CommandParser;
/**
 * Todo: add unit test
 */
public class LineCommandParser extends CommandParser<Line> {

    private static final String FORMAT_ERROR = "Line command invalid expecting [L X Y X Y] example: L 1 5 3 5";
    public static final String COMMAND_FORMAT = "L [0-9]+ [0-9]+ [0-9]+ [0-9]+";

    public LineCommandParser(){
        super(COMMAND_FORMAT, FORMAT_ERROR);
    }

    @Override
    public Line parse(final String command) {
        checkFormat(command);

        final String[] split = command.split(" ");
        int p1x = Integer.parseInt(split[1]);
        int p1y = Integer.parseInt(split[2]);
        int p2x = Integer.parseInt(split[3]);
        int p2y = Integer.parseInt(split[4]);

        return Line.of(Point.of(p1x, p1y), Point.of(p2x, p2y));
    }
}
