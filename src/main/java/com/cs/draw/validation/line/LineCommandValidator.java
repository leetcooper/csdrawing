package com.cs.draw.validation.line;

import com.cs.draw.model.Line;
import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.ApplicationErrors;
import com.cs.draw.validation.CommandValidator;

import java.util.Objects;

public class LineCommandValidator extends CommandValidator<Line> {
    @Override
    public ApplicationErrors validate(final Line line){
        Objects.requireNonNull(line);
        return (!line.isAcross()) ? ApplicationErrors.empty() :
               ApplicationErrors
                       .of(ApplicationError.of("LP", "Only horizontal and vertical line supported"));
    }
}


