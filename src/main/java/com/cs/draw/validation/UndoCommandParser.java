package com.cs.draw.validation;

import com.cs.draw.model.Undo;

public class UndoCommandParser extends CommandParser<Undo> {
    public static final String FORMAT_ERROR = "U";
    public static final String COMMAND_FORMAT = "U";

    public UndoCommandParser() {
        super(COMMAND_FORMAT, FORMAT_ERROR);
    }

    @Override
    public Undo parse(String command) {
        return new Undo();
    }
}
