package com.cs.draw.validation.rectangle;

import com.cs.draw.model.Dimension;
import com.cs.draw.model.Point;
import com.cs.draw.model.Rectangle;
import com.cs.draw.validation.CommandParser;
/**
 * Todo: add unit test
 */
public class RectangleCommandParser extends CommandParser<Rectangle> {

    public static final String FORMAT_ERROR =
            "Rectangle invalid expecting [R TLX TLY BRX BRY] \" +\n" +
                    "                    \"example: R 10 20 20 30\"";
    public static final String COMMAND_FORMAT = "R [0-9]+ [0-9]+ [0-9]+ [0-9]+";

    public RectangleCommandParser(){
        super(COMMAND_FORMAT, FORMAT_ERROR);
    }

    @Override
    public Rectangle parse(final String command) {
        checkFormat(command);

        final String[] split = command.split(" ");

        int x1 = Integer.parseInt(split[1]);
        int y1 = Integer.parseInt(split[2]);
        int x2 = Integer.parseInt(split[3]);
        int y2 = Integer.parseInt(split[4]);

        int minX = findMin(x1,x2);
        int minY = findMin(y1,y2);
        int maxX = findMax(x1,x2);
        int maxY = findMax(y1,y2);

        final Point rectanglePoint = Point.of(minX, minY);

        final Dimension dimension =
                Dimension.of((maxX+1)-rectanglePoint.getX(),
                        (maxY+1)-rectanglePoint.getY());

        return Rectangle.of(rectanglePoint,dimension,
                Rectangle.RectangleCharacters.of("*", "*"));
    }

    private int findMin(int i1, int i2) {
        return i1 < i2 ? i1 : i2;
    }

    private int findMax(int i1, int i2) {
        return i1 > i2 ? i1 : i2;
    }

    private static Point getRectanglePoint(String command) {
        final String[] split = command.split(" ");

        int x = Integer.parseInt(split[1]);
        int y = Integer.parseInt(split[2]);

        return Point.of(x, y);
    }

}
