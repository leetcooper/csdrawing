package com.cs.draw.validation.rectangle;

import com.cs.draw.model.Rectangle;
import com.cs.draw.validation.ApplicationError;
import com.cs.draw.validation.ApplicationErrors;
import com.cs.draw.validation.CommandValidator;

import java.util.Objects;

public class RectangleCommandValidator extends CommandValidator<Rectangle> {
    public static final String FORMAT_ERROR =
            "Rectangle invalid expecting [R TLX TLY BRX BRY] \" +\n" +
                    "                    \"example: R 10 20 20 30\"";
    @Override
    public ApplicationErrors validate(final Rectangle rectangle){
        Objects.requireNonNull(rectangle);

        /*
        if(rectangle.getDimension().getH() <= 0 || rectangle.getDimension().getW() <= 0)
            return ApplicationErrors.of(ApplicationError.of("NR", FORMAT_ERROR));
        */

        return ApplicationErrors.empty();
    }
}


