package com.cs.draw.validation;

public interface Validator<T> {
    ApplicationErrors validate(T object);
}
